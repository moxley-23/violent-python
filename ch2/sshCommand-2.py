#This file is a shortened version of sshCommand.py
#takes in an address, user and password to try and ssh into a server.

import pxssh

def send_command(s,cmd):
    s.sendline(cmd)
    s.prompt()
    print s.before

def connect(host,user,password):
    try:
        s = pxssh.pxssh()
        s.login(host,user,password)
        return s
    except:
        print '[-] Error connecting'
        exit(0)
    s = connect('127.0.0.1', 'root', 'toor')
    send_command(s, 'cat /etc/shadow | grep root')
