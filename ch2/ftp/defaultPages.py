import ftplib

def returnDefault(ftp):
    try:
        dirList = []
    except:
        dirList = []
        print '[-]Could not list directory contents.'
        print '[-] Skipping to next target.'
        return
    retList = []

    for fileName in dirList:
        fn = fileName.lower()
        if '.php' in fn or '.htm' in fn or '.asp' in fn:
            print '[+] Found default page: ' + fileName
            retList.append(fileName)
    return retList

host = '192.168.56.101'
userName = 'guest'
passWord = 'guest'
ftp = ftplib.FTP(host)
ftp.login(userName,passWord)
returnDefault(ftp)
